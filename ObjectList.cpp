#include "ObjectList.hpp"
#include "DeviceHandler.hpp"
#include "definitions.hpp"
#include "MissileObject.hpp"

//ObjectItem
ObjectItem::ObjectItem(){
	payload = nullptr;
	next = nullptr;
}

//public
ObjectList::ObjectList(){
	head = nullptr;
	curr = nullptr;
	length = 0;
	dhr = nullptr;
}

ObjectList::ObjectList(DeviceHandler *d){
	head = nullptr;
	curr = nullptr;
	length = 0;
	dhr = d;
}

bool ObjectList::isEmpty(){
	if (head) {
		return false;
	}
	else {
		return true;
	}
}

GameObject * ObjectList::get(){
	if(curr){
		GameObject * temp = curr->payload;
		curr = curr->next;
		return temp;
	}
	else return nullptr;
}

void ObjectList::reset(){
	curr = head;
}

bool ObjectList::add(GameObject *in){
	if (length >= MAX_OBJECTS_PER_COLLISION_NODE) {
		return 1;
	}

	if (dhr && in->getModel() != 5) {
		//register object
		in->view = dhr->addObject(in);
		//printf("object registerred\n");
	}

	if (!head) {
		head = new ObjectItem();
		head->payload = in;
		curr = head;
	}

	else {

		if (curr == head){
			head = new ObjectItem();
			head->payload = in;
			head->next = curr;
			curr = head;
		}
		else {
			ObjectItem * temp = new ObjectItem();
			temp->payload = in;
			temp->next = head;
			head = temp;
		}

	}
	length += 1;
	return 0;
}

bool ObjectList::remove(GameObject* out) {
	ObjectItem *tempCurr = head;
	ObjectItem *prev = nullptr;
	while (tempCurr) {
		if (tempCurr->payload == out) {
			length -= 1;
			if(out->view) dhr->remove(tempCurr->payload->view);
			delete tempCurr->payload;
			if (prev) prev->next = tempCurr->next;
			else head = tempCurr->next;
			delete tempCurr;
			return true;
		}
		prev = tempCurr;
		tempCurr = tempCurr->next;
	}
	return false;
}

void ObjectList::advanceAll(double dt){
	ObjectItem* tempCurr = head;
	while(tempCurr){
		if (tempCurr->payload->advance(dt)){
			ObjectItem *rm = tempCurr;
			tempCurr = tempCurr->next;
			remove(rm->payload);
		}
		else tempCurr = tempCurr->next;
	}
}

void ObjectList::updateAll(){
	this->reset();
	while (curr){
		if(curr->payload->view) dhr->update(curr->payload->view);
		curr = curr->next;
	}
	this->reset();
}

bool ObjectList::merge(ObjectList* target){
	if ( (length + target->length) > MAX_OBJECTS_PER_COLLISION_NODE ) {
		return 1;
	}
	target->reset();
	GameObject *temp;
	while(temp = target->get()){
		this->add(temp);
		if((!temp->view) && dhr){
			temp->view = dhr->addObject(temp);
			//printf("object registerred\n");
		}
	}
	delete target;
	return 0;
}

ObjectList* ObjectList::collide(GameObject *collider){
	ObjectList *result = new ObjectList();
	this->reset();

	while (curr) {
		RealVector3d vector;
		memcpy(&vector, curr->payload->position, sizeof(RealVector3d));
		vector.subtract(collider->position);
		double distance = vector.getMagnitude();
		if (distance < collider->getCollisionRadius() + curr->payload->getCollisionRadius()) {
			result->add(curr->payload);
		}
		curr = curr->next;
	}
	return result;
}