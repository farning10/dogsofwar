#ifndef DEVICEHANDLER_H
#define DEVICEHANDLER_H

#include <irrlicht.h>
#include "local.hpp"

using namespace irr;

struct Item
{
	Item * next;
	scene::ISceneNode * payload;
	GameObject * gameObject;
	Item();
};

class DeviceHandler{
public:
	DeviceHandler(int, int, bool);
	//functions to display the shit
	bool deviceExists();
	video::IVideoDriver * getDriver();
	scene::ISceneManager * getSceneManager();
	void setMouseInvisible();
	bool run();
	u32 getTime();
	gui::IGUIEnvironment * getEnvironment();
	void stopAll();
	void setup(); //only meant to be called once
	void draw();
	scene::ISceneNode * getNode();
	ControlInputReceiver * getReceiver();
	Item * addObject(GameObject*);
	void update(Item*);
	void drawLine(RealVector3d*, RealVector3d*); // do not use yet
	void remove(Item*);

private:
	void insert(Item*);
	void setupGUI(); //only meant to be called once
	IrrlichtDevice * device;
	video::IVideoDriver * driver;
	scene::ISceneManager * smgr;
	ControlInputReceiver * cir;
	scene::ICameraSceneNode * camera;
	float camFOV; //radians lol
	gui::IGUIEnvironment * env;
	Item * listHead;
	Item * listTail;
	scene::IMesh *planeMesh;
	scene::IMesh *bulletMesh;
	video::ITexture *planeTexture;
	video::ITexture *planeTextureTransparent;
	video::ITexture *bulletTexture;
	video::ITexture *boxTexture;
};

#endif