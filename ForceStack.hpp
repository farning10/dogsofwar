#ifndef FORCESTACK_H
#define FORCESTACK_H

#include "GameObject.hpp"

struct Force
{
	Force(RealVector3d *force, RealVector3d *arm);
	Force();
	RealVector3d force;
	RealVector3d arm;
};

struct ForceItem
{
	ForceItem(RealVector3d *force, RealVector3d *arm);
	ForceItem(Force *);
	Force *force;
	ForceItem *next;
};

class ForceStack
{
public:
	ForceStack();
	void push(RealVector3d *force, RealVector3d *arm);
	void push(Force *);
	Force *pop();

private:
	ForceItem *head;
};

#endif