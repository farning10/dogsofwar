#include "PlayerObject.hpp"

PlayerObject::PlayerObject(RealVector3d* startPos, DeviceHandler* dh)
: PlaneObject(startPos)
{
	cameraPresent = true;
	cameraAngle = new RealVector3d(0, 0, -1);
	cameraDistance = CAMERA_BASE_DISTANCE;
	gunCam = false;

	deviceHandler = dh;
	increment = 0.5f;
	offset = 0.5f;
	int i;
	for(i = 0; i < NUMBER_IN_LINE; i++){
		RealVector3d start(0,0,0);
		line[i] = new PlaneObject(&start);
		line[i]->setModel(3);
		lineViews[i] = deviceHandler->addObject(line[i]);
	}

	missiles = nullptr;
	selectDSMS = 0;
	RealVector3d pylon1(6, -3, 0);
	RealVector3d pylon2(-6, -3, 0);
	MissileObject *add = new MissileObject(this->position);
	Pylon *newPylon = new Pylon(&pylon1, add);
	memcpy(&payload[0], newPylon, sizeof(Pylon));
	add = new MissileObject(this->position);
	newPylon = new Pylon(&pylon2, add);
	memcpy(&payload[1], newPylon, sizeof(Pylon));
}

int PlayerObject::firePayload(){
	Pylon *selected = &payload[selectDSMS];
	if(!selected->ready()) return 1;
	MissileObject *toFire = selected->load;
	RealVector3d launchPOS;
	memcpy(&launchPOS, this->position, sizeof(RealVector3d));
	launchPOS.add(&selected->position);
	memcpy(toFire->position, &launchPOS, sizeof(RealVector3d));
	memcpy(toFire->velocity, this->velocity, sizeof(RealVector3d));
	toFire->rotateQuat(this->rotation);
	missiles->add(toFire);
	selected->load = nullptr;
	return 0;
}

int PlayerObject::lockOn(GameObject *target){
	Pylon *selected = &payload[selectDSMS];
	if(!selected->load) return 1;
	selected->load->setTarget(target);
	return 0;
}

void PlayerObject::initializeMissiles(ObjectList *missileList, ObjectList *bulletList){
	missiles = missileList;
	int i;
	for (i = 0; i < numberOfPylons; i++){
		payload[i].load->bulletList = bulletList;
	}
}

int PlayerObject::advance(double dt){
	PlaneObject::advance(dt);
	offset = fmod(offset + dt, increment);
	return 0;
}

void PlayerObject::setLine(){
	double tempOffset = offset;
	int i;
	PlaneObject * prev = this;
	for(i = 0; i < NUMBER_IN_LINE; i++){
		line[i]->deepCopy(prev);

		line[i]->advance(increment-tempOffset);
		deviceHandler->update(lineViews[i]);
		prev = line[i];
		tempOffset = 0;
		//if(i > 0) deviceHandler->drawLine(line[i]->position, line[i-1]->position);
	}
}

void PlayerObject::switchDSMS(){
	selectDSMS++;
	selectDSMS = selectDSMS % numberOfPylons;
	printf("%d\n", selectDSMS);
}


