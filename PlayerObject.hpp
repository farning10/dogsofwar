#ifndef PLAYEROBJECT_H
#define PLAYEROBJECT_H

#include "local.hpp"

class DeviceHandler;
class PlaneObject;

class PlayerObject : public PlaneObject{
public:
	PlayerObject(RealVector3d*, DeviceHandler*);
	void setLine();
	virtual int advance(double);
	virtual int firePayload();
	virtual int lockOn(GameObject *target);
	virtual void switchDSMS();
	void initializeMissiles(ObjectList *missiles, ObjectList *bullets);

	RealVector3d * cameraAngle;
	double cameraDistance;
	bool gunCam;
	
protected:
	DeviceHandler * deviceHandler; //device handler for the scene
	double increment; //amount of time to next draw in seconds
	double offset;
	PlaneObject * line[NUMBER_IN_LINE]; //array of aim line objects
	Item * lineViews[NUMBER_IN_LINE]; //array of views for the aim line objects
	Pylon payload[2];//array containg the pylons in the plane
	const int numberOfPylons = 2;
	int selectDSMS;
	ObjectList *missiles;//the main missile list
};

#endif