CC = g++ -std=c++11
OBJECTS = obj/Game.o obj/ObjectList.o obj/ControlInputReceiver.o obj/DeviceHandler.o obj/GameObject.o obj/PlaneObject.o obj/PlayerObject.o obj/BulletObject.o obj/AIObject.o obj/MissileObject.o obj/BlastObject.o obj/ForceStack.o obj/main.o
HEADERS = Game.hpp GameObject.hpp GameSettings.hpp ObjectList.hpp PlaneObject.hpp ControlInputReceiver.hpp DeviceHandler.hpp BulletObject.hpp PlayerObject.hpp AIObject.hpp MissileObject.hpp BlastObject.hpp ForceStack.hpp
IRR = -IC:/C++Libraries/IRRlicht/include -LC:/C++Libraries/IRRlicht/lib/Win32-gcc \
 -IC:\C++Libraries\irrlicht-1.8.1\include -LC:\C++Libraries\irrlicht-1.8.1\lib\Win32-gcc -lIrrlicht
FLAGS = -g
LINKERFLAGS = # -static -static-libgcc -static-libstdc++
FINAL = bin/DogsOfWar.exe

all: $(FINAL)

$(FINAL): $(OBJECTS) $(HEADERS)
	$(CC) $(IRR) $(FLAGS) -o $(FINAL) $(OBJECTS) $(IRR) $(LINKERFLAGS)

obj/Game.o: Game.cpp
	$(CC) $(IRR) $(FLAGS) -c Game.cpp -o obj/Game.o $(LINKERFLAGS)

obj/ObjectList.o: ObjectList.cpp
	$(CC) $(IRR) $(FLAGS) -c ObjectList.cpp -o obj/ObjectList.o $(LINKERFLAGS)

obj/ControlInputReceiver.o: ControlInputReceiver.cpp
	$(CC) $(IRR) $(FLAGS) -c ControlInputReceiver.cpp -o obj/ControlInputReceiver.o $(LINKERFLAGS)

obj/DeviceHandler.o: DeviceHandler.cpp
	$(CC) $(IRR) $(FLAGS) -c DeviceHandler.cpp -o obj/DeviceHandler.o $(LINKERFLAGS)

obj/ForceStack.o: ForceStack.cpp
	$(CC) $(IRR) $(FLAGS) -c ForceStack.cpp -o obj/ForceStack.o $(LINKERFLAGS)

obj/GameObject.o: GameObject.cpp
	$(CC) $(IRR) $(FLAGS) -c GameObject.cpp -o obj/GameObject.o $(LINKERFLAGS)

obj/PlaneObject.o: PlaneObject.cpp
	$(CC) $(IRR) $(FLAGS) -c PlaneObject.cpp -o obj/PlaneObject.o $(LINKERFLAGS)

obj/PlayerObject.o: PlayerObject.cpp
	$(CC) $(IRR) $(FLAGS) -c PlayerObject.cpp -o obj/PlayerObject.o $(LINKERFLAGS)

obj/BulletObject.o: BulletObject.cpp
	$(CC) $(IRR) $(FLAGS) -c BulletObject.cpp -o obj/BulletObject.o $(LINKERFLAGS)

obj/AIObject.o: AIObject.cpp
	$(CC) $(IRR) $(FLAGS) -c AIObject.cpp -o obj/AIObject.o $(LINKERFLAGS)

obj/MissileObject.o: MissileObject.cpp
	$(CC) $(IRR) $(FLAGS) -c MissileObject.cpp -o obj/MissileObject.o $(LINKERFLAGS)

obj/BlastObject.o: BlastObject.cpp
	$(CC) $(IRR) $(FLAGS) -c BlastObject.cpp -o obj/BlastObject.o $(LINKERFLAGS)

obj/main.o: main.cpp
	$(CC) $(IRR) $(FLAGS) -c main.cpp -o obj/main.o $(LINKERFLAGS)

.PHONY: clean
clean: 
	rm $(OBJECTS) $(FINAL)
