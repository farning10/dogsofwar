#ifndef OCTREE_H
#define OCTREE_H

struct OctreeNode {
	OctreeNode *parent;
	OctreeNode *children[8];
	ObjectList objects;
	DeviceHandler *dhr;
	RealVector3d midpoint;
	double sideLength;

	OctreeNode(DeviceHandler*);
	OctreeNode(DeviceHandler*, OctreeNode* parent);
	void subDivide();
};

class Octree {
public:
	Octree(DeviceHandler*);
	void add(GameObject*);
private:
	void addWalk(OctreeNode* start, GameObject*);
	OctreeNode *root;
};

#endif