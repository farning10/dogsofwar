#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <math.h>
#include <stdio.h>
#include <string.h>

struct Item;
struct RealVector4d;
struct RealMatrix3d;
class ObjectList;


struct RealVector3d{
	RealVector3d();
	RealVector3d(double, double, double);
	void normalize();
	RealVector3d * getNormalize();
	void rotate(RealMatrix3d*);
	void rotateCongruent(RealMatrix3d*);
	void rotateQuat(RealVector4d*);
	void reflect(RealVector3d*);
	void add(RealVector3d*);
	void subtract(RealVector3d*);
	void multiply(double);
	RealVector3d *getMultiply(double);
	RealVector3d *copy();
	double getMagnitude();
	static double dotProduct(RealVector3d*, RealVector3d*);
	static RealVector3d * crossProduct(RealVector3d*, RealVector3d*);
	static double angleBetween(RealVector3d*, RealVector3d*);
	static double angleVectorPlane(RealVector3d*, RealVector3d*);
	static void copy(RealVector3d*, RealVector3d*); //destination, source

	double dx;
	double dy;
	double dz;
};

struct RealVector4d{
	static RealVector4d * axisAngle(RealVector3d*, double);

	RealVector4d();
	void normalize();
	void operate(RealVector4d*);
	RealMatrix3d * getMatrix();

	double dx;
	double dy;
	double dz;
	double w;
};

struct RealMatrix3d{
	RealMatrix3d();
	double contents[3][3];
};

class GameObject{
public:
	GameObject();
	GameObject(RealVector3d*);
	void reset();
	void move(RealVector3d*);
	void rotateQuat(RealVector4d*);
	void rotate(RealVector3d *, double);
	virtual int advance(double);
	int getModel();
	void setModel(int);
	double getCollisionRadius();

//abstracted
	RealVector3d *xAxis; //right
	RealVector3d *yAxis; //up
	RealVector3d *zAxis; //forward
	RealVector3d *position;
	RealVector4d *rotation;
	Item *view;
	bool cameraPresent; //true if the main camera should focus on this object

//physical
	RealVector3d *velocity; //meters per second relative to plane
protected:
	RealVector3d *acceleration; //meters per second^2 reletive to plane
	double mass; //kilograms
	int model; //0 no model, 1 plane, 2 bullet, 3 transparent plane
	double collisionRadius; //meters sphere for collisions
};

#endif