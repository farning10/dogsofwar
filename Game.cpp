#include "Game.hpp"


//public:
Game::Game(DeviceHandler* d)
: bullets(d), planes(d), missiles(d)
{ //simple constructor
	deviceHandler = d;
	cir = deviceHandler->getReceiver();

	deviceHandler->setup();
	RealVector3d startPoint(0, 0, 0); //creates a playerObject for the player to control
	player = new PlayerObject(&startPoint, deviceHandler);
	player->initializeMissiles(&missiles, &bullets);
	planes.add(player);
	RealVector3d bogieStart(0, 0, 1000);
	AIObject *bogie = new AIObject(&bogieStart, &planes, &bullets);
	//MissileObject *bogie = new MissileObject(&bogieStart);
	//bogie->bulletList = &bullets;
	//bogie->setTarget(player);
	//missiles.add(bogie);
	planes.add(bogie);

	rotSpeed = 1.f; //radians per second for manual rotation

	camCheck = true; //booleans to keep track of button positions
	spaceCheck = true;
	switchCheck = true;
	lockCheck = true;
	rCheck = true;
	physics = false;

	currMS = cir->getMouseState(); // structs to keep track of the state of the mouse
	prevMS = currMS;
}

void Game::play(){
	unsigned int then = deviceHandler->getTime();
	int stop = 0;

	while(!stop){
//		printf("%d\n", then);
		const unsigned int now = deviceHandler->getTime(); //time elapsed info
//		printf("%d\n", now);
		const double frameDeltaTime = (f32)(now - then) / 1000.f;
//		if (frameDeltaTime > 0) printf("%lf\n", frameDeltaTime);
		stop = this->cycle(frameDeltaTime);
		then = now;
	}

	deviceHandler->stopAll();
}

//protected:
int Game::cycle(double dt){ // returns 0 on success; 1, 2, or 3 on failure

	if ( !player ) return 5;
	if ( !deviceHandler->run() ) return 4;
	if ( input(dt) ) return 1;
	if ( advanceGame(dt) ) return 2;
	if ( render() ) return 3;

	return 0;
}

int Game::input(double frameDeltaTime){
	//check for input, execute anything input related
	ControlSet input((double)cir->scrollBarCheck(GUI_ID_THROTTLE) / 1000.f,
					 (double)(cir->scrollBarCheck(GUI_ID_PITCH) - 500) / -500.f,
					 (double)(cir->scrollBarCheck(GUI_ID_ROLL) - 500) / -500.f,
					 (double)(cir->scrollBarCheck(GUI_ID_YAW) - 500) / -500.f);
	player->setControl(&input);

	prevMS = currMS;
	currMS = cir->getMouseState();

	if(currMS.rightButtonDown){  //camera rotation
		RealVector3d yaxis(0, 1, 0);
		RealVector3d * xaxis = RealVector3d::crossProduct(player->cameraAngle, &yaxis);

		RealVector4d * temp = RealVector4d::axisAngle(&yaxis, (double)((currMS.positionX - prevMS.positionX)*CAMERA_ROTATION));
		RealMatrix3d * tempMatrix = temp->getMatrix();
		player->cameraAngle->rotate(tempMatrix);
		delete temp;
		delete tempMatrix;

		temp = RealVector4d::axisAngle(xaxis, (double)((currMS.positionY - prevMS.positionY)*CAMERA_ROTATION));
		tempMatrix = temp->getMatrix();
		player->cameraAngle->rotate(tempMatrix);
		delete temp;
		delete tempMatrix;
		delete xaxis;
	}

	double distance = pow(CAMERA_ZOOM, currMS.scrollTotal);
	player->cameraDistance = distance*CAMERA_BASE_DISTANCE;

	if(cir->IsKeyDown(irr::KEY_NUMPAD8)){
		player->rotate(player->xAxis, rotSpeed*frameDeltaTime);
		deviceHandler->update(player->view);
	}

	if(cir->IsKeyDown(irr::KEY_NUMPAD2)){
		player->rotate(player->xAxis, -rotSpeed*frameDeltaTime);
		deviceHandler->update(player->view);
	}

	if(cir->IsKeyDown(irr::KEY_NUMPAD6)){
		player->rotate(player->zAxis, -rotSpeed*frameDeltaTime*2);
		deviceHandler->update(player->view);
	}

	if(cir->IsKeyDown(irr::KEY_NUMPAD4)){
		player->rotate(player->zAxis, rotSpeed*frameDeltaTime*2);
		deviceHandler->update(player->view);
	}

	if(cir->IsKeyDown(irr::KEY_NUMPAD3)){
		player->rotate(player->yAxis, rotSpeed*frameDeltaTime);
		deviceHandler->update(player->view);
	}

	if(cir->IsKeyDown(irr::KEY_NUMPAD1)){
		player->rotate(player->yAxis, -rotSpeed*frameDeltaTime);
		deviceHandler->update(player->view);
	}

	if(cir->IsKeyDown(irr::KEY_KEY_N)){
		if(physics){
			ObjectList *tempList = player->fireInternal(frameDeltaTime);
			bullets.merge(tempList);

		}
	}

	if(cir->IsKeyDown(irr::KEY_KEY_B)){
		if(physics){
			if(player->firePayload()) printf("Firing failed\n");
			else printf("Fired missile\n");
		}
	}

	if(cir->IsKeyDown(irr::KEY_KEY_V)){
		if(lockCheck){
			GameObject *curr;
			planes.reset();
			while(curr = planes.get()){
				if(curr != player) player->lockOn(curr);
			}
			lockCheck = false;
		}
	}
	else {
		if (!lockCheck) {
			lockCheck = true;
		}
	}

	if(cir->IsKeyDown(irr::KEY_KEY_C)){
		if(switchCheck){
			if (player) player->switchDSMS();
			switchCheck = false;
		}
	}
	else {
		if (!switchCheck) {
			switchCheck = true;
		}
	}

	if(cir->IsKeyDown(irr::KEY_KEY_M)){
		if(camCheck){
			player->gunCam = !player->gunCam;
			camCheck = false;
		}
	}
	else {
		if (!camCheck) {
			camCheck = true;
		}
	}

	if(cir->IsKeyDown(irr::KEY_SPACE)){
		if(spaceCheck){
			physics = !physics;
			spaceCheck = false;
		}
	}
	else{
		if(!spaceCheck){
			spaceCheck = true;
		}
	}

	if(cir->IsKeyDown(irr::KEY_KEY_R)){
		if(rCheck){
			player->reset();
			rCheck = false;
		}
	}
	else{
		if(rCheck){
			rCheck = true;
		}
	}

	if(cir->IsKeyDown(irr::KEY_ESCAPE)) return 1;
	return 0;
}


int Game::advanceGame(double frameDeltaTime){
	//advance the physics of the game
	if(physics){
//		player->advance(frameDeltaTime);
		planes.advanceAll(frameDeltaTime);
		bullets.advanceAll(frameDeltaTime);
		missiles.advanceAll(frameDeltaTime);
		planes.reset();
		GameObject *curr;
		while (curr = planes.get()) {
			ObjectList *collisions = bullets.collide(curr);
			if (!collisions->isEmpty()){
				planes.remove(curr);
				if(player == curr){
					player = nullptr;
				}
			}
			delete collisions;
		}
	}
	return 0;
}


int Game::render(){
	//draw all of the game objects and terrain and shit with Irrlicht
//	deviceHandler->update(player->view);
	planes.updateAll();
	bullets.updateAll();
	missiles.updateAll();
	if(player) player->setLine();
	deviceHandler->draw();
	return 0;
}