#ifndef BLASTOBJECT_H
#define BLASTOBJECT_H

#include "GameObject.hpp"

class BlastObject : public GameObject {
public:
	BlastObject(RealVector3d*, double, double);
	virtual int advance(double);
protected:
	double maxKillRadius;
	double explosionDuration;
};

#endif