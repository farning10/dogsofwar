#ifndef OBJECTLIST_H
#define OBJECTLIST_H

#include "GameObject.hpp"

class DeviceHandler;

struct ObjectItem {
	ObjectItem();
	GameObject *payload;
	ObjectItem *next;
};

class ObjectList {
public:
	ObjectList();
	ObjectList(DeviceHandler*);
	bool isEmpty();
	GameObject* get();
	void reset();
	bool add(GameObject*);
	bool remove(GameObject*);
	void advanceAll(double);
	void updateAll();
	bool merge(ObjectList*);
	ObjectList* collide(GameObject*);

	long length;
private:
	ObjectItem *head;
	ObjectItem *curr;
	DeviceHandler *dhr;
};

#endif