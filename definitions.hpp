#ifndef DEFINITIONS_H
#define DEFINITIONS_H

//#define DEBUG
//#define LOW_END
//#define LAPTOP

#ifndef DEBUG
#define FULLSCREEN true

#ifdef LOW_END
#define HEIGHT 900
#define WIDTH 1600
#define ANTI_ALIASING 0
#define RENDER_DISTANCE 2000

#else
#ifdef LAPTOP
#define HEIGHT 900
#define WIDTH 1440
#define ANTI_ALIASING 2
#define RENDER_DISTANCE 4000

#else
#define HEIGHT 1080
#define WIDTH 1920
#define ANTI_ALIASING 4
#define RENDER_DISTANCE 5000
#endif
#endif

#else
#define HEIGHT 405
#define WIDTH 720
#define FULLSCREEN false
#define ANTI_ALIASING 0
#define RENDER_DISTANCE 2000
#endif

#define CAMERA_ROTATION 0.005
#define CAMERA_ZOOM 1.1
#define CAMERA_BASE_DISTANCE 100

#define M_PI 3.14159265358979323846  /* pi */
#define MOVEMENT_PRECISION 0.01

#define NUMBER_IN_LINE 10

#define MAX_OBJECTS_PER_COLLISION_NODE 1000

#define NUMBER_OF_SCROLL_BARS 4
enum {
	GUI_ID_THROTTLE, // 0
	GUI_ID_PITCH,	 // 1
	GUI_ID_ROLL,	 // 2 and so on...
	GUI_ID_YAW
};

#endif