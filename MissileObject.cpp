#include "MissileObject.hpp"
#include "BlastObject.hpp"

MissileObject::MissileObject(RealVector3d *startPoint)
: PlaneObject(startPoint), prevLOS()
{
	model = 4;
	mass = 152;
	maxThrust = 16000;
	thrust = 0;
	wingArea = 0.1f;
	tailArea = 0.1f;
	mPitch = 7.f;
	mRoll = 7.f;
	mYaw = 7.f;
	burnDuration = 25.f;
	delay = 1.5f;
	burnStarted = false;
	currTime = 0.f;
	fuelMass = 72.f;
	blastRadius = 50.f;
	target = nullptr;
	averageStaticDrag = 0.1f;
	armed = true;
}

void MissileObject::setTarget(GameObject *newTarget){
	target = newTarget;
	memcpy(&prevLOS, target->position, sizeof(RealVector3d));
	prevLOS.subtract(this->position);
}

ObjectList* MissileObject::fireInternal(double dt){
	return nullptr;
}

int MissileObject::advance(double dt){
	this->burnFuel(dt);
	ControlSet *currControls = this->align();
	this->setControl(currControls);
	PlaneObject::advance(dt);
	return fuse();
}

ControlSet *MissileObject::align(){
	ControlSet *result = new ControlSet();

	// RealVector3d LOS;
	// memcpy(&LOS, target->position, sizeof(RealVector3d));
	// // RealVector3d deltaLOS;
	// // memcpy(&deltaLOS, &LOS, sizeof(RealVector3d));
	// // deltaLOS.subtract(&prevLOS);
	// RealVector3d *axis = RealVector3d::crossProduct(&prevLOS, &LOS);
	// double angle = RealVector3d::angleBetween(&prevLOS, &LOS);

	// printf("angle bteween: %f\n", angle);

	// this->rotate(axis, angle/2000);
	// delete axis;

	// memcpy(&prevLOS, &LOS, sizeof(RealVector3d));


	RealVector3d path;
	memcpy(&path, target->position, sizeof(RealVector3d));
	path.subtract(position);

	RealVector3d lead;
	memcpy(&lead, target->velocity, sizeof(RealVector3d));
	lead.multiply(path.getMagnitude()/this->velocity->getMagnitude());
	path.add(&lead);

	RealVector3d *ppath = &path;

	double angle = acos(RealVector3d::dotProduct(ppath, yAxis) / (path.getMagnitude()*yAxis->getMagnitude())); //in rads
	RealVector3d *axis = RealVector3d::crossProduct(ppath, zAxis);
	double rotationAngle = acos(RealVector3d::dotProduct(axis, yAxis) / (axis->getMagnitude()*xAxis->getMagnitude()));
	double dRot = (rotationAngle - (M_PI/2)) / M_PI;
	double dPitch = (angle - (M_PI/2)) / -M_PI;
	if (rotationAngle == rotationAngle) {
		result->roll = 2*dRot/M_PI;
		if (result->roll > 1) result->roll = 1;
		if (result->roll < -1) result->roll = -1;
		result->pitch = 2*dPitch/M_PI;
		if (result->pitch > 1) result->pitch = 1;
		if (result->pitch < -1) result->pitch = -1;
		if(dRot > 0.00005) {
			result->roll = dRot/(M_PI*4) + 0.5;
		}
		else if (dRot < 0.00005) {
			result->roll = dRot/(M_PI*4) - 0.5;
		}
		else result->roll = 0;
	}
	if (angle == angle) {
		if(dPitch > 0.00005 ) {
			result->pitch = dRot/(M_PI*4) + 0.5;
		}
		else if(dPitch < 0.00005 ) {
			result->pitch = dRot/(M_PI*4) - 0.5;
		}
		else result->pitch = 0;
	}
	result->throttle = 1;
	delete axis;

	// printf("%f, %f | %f\n", target->velocity->getMagnitude(), this->velocity->getMagnitude(), this->currTime);

	return result;
}

void MissileObject::burnFuel(double dt){
	if (delay > 0) {
		thrust = 0;
		delay -= dt;
	}
	else if (!burnStarted) {
		burnStarted = true;
		thrust = maxThrust;
	}
	if (thrust) {
		mass -= (fuelMass/burnDuration)*dt;
		currTime += dt;
		if (currTime >= burnDuration) thrust = 0;
	}
}

int MissileObject::fuse(){
	RealVector3d LOS;
	memcpy(&LOS, target->position, sizeof(RealVector3d));
	LOS.subtract(this->position);
	double distance = LOS.getMagnitude();
	if (distance < 30){
		if(bulletList){
			bulletList->add(new BlastObject(this->position, blastRadius, 0.1));
		}
		return 2;
	}
	else return 0;
}

bool MissileObject::readyToFire(){
	if(target) return true;
	else return false;
}