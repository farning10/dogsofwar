#include "BlastObject.hpp"

BlastObject::BlastObject(RealVector3d *centerPoint, double radius, double duration){
	position = new RealVector3d();
	memcpy(position, centerPoint, sizeof(RealVector3d));
	model = 5;
	view = nullptr;
	collisionRadius = 0;
	xAxis = nullptr;
	yAxis = nullptr;
	zAxis = nullptr;
	rotation = nullptr;
	cameraPresent = false;
	maxKillRadius = radius;
	explosionDuration = duration;
}

int BlastObject::advance(double dt){
	collisionRadius += maxKillRadius*dt / explosionDuration;
	if (collisionRadius >= maxKillRadius) return 1;
	else return 0;
}