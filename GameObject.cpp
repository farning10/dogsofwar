#include "GameObject.hpp"

RealVector3d::RealVector3d(){
	dx = 0.f;
	dy = 0.f;
	dz = 0.f;
}

RealVector3d::RealVector3d(double x, double y, double z){
	dx = x;
	dy = y;
	dz = z;
}

void RealVector3d::normalize(){
	double magnitude = sqrt(dx*dx + dy*dy + dz*dz);
	if(magnitude != 0){
		dx /= magnitude;
		dy /= magnitude;
		dz /= magnitude;
	}
}

RealVector3d * RealVector3d::getNormalize(){
	double magnitude = sqrt(dx*dx + dy*dy + dz*dz);
	RealVector3d * result = new RealVector3d(dx, dy, dz);
	if(magnitude != 0){
		result->dx /= magnitude;
		result->dy /= magnitude;
		result->dz /= magnitude;
	}
	return result;
}

void RealVector3d::rotate(RealMatrix3d *o){
	this->rotateCongruent(o);
	this->normalize();
}

void RealVector3d::rotateCongruent(RealMatrix3d *o){
	double tempdx = dx;
	double tempdy = dy;
	double tempdz = dz;

	dx = tempdx*o->contents[0][0] + tempdy*o->contents[1][0] + tempdz*o->contents[2][0];
	dy = tempdx*o->contents[0][1] + tempdy*o->contents[1][1] + tempdz*o->contents[2][1];
	dz = tempdx*o->contents[0][2] + tempdy*o->contents[1][2] + tempdz*o->contents[2][2];
}

void RealVector3d::rotateQuat(RealVector4d *oper8er){
	RealMatrix3d *operMatrix = oper8er->getMatrix();
	this->rotateCongruent(operMatrix);
	delete operMatrix;
}

void RealVector3d::reflect(RealVector3d * n){
	double dp = dotProduct(this, n) * 2;
	RealVector3d * result = n->getMultiply(dp);
	this->multiply(-1);
	result->add(this);
	dx = result->dx;
	dy = result->dy;
	dz = result->dz;
	delete result;
}

void RealVector3d::add(RealVector3d * addend) {
	dx += addend->dx;
	dy += addend->dy;
	dz += addend->dz;
}

void RealVector3d::subtract(RealVector3d* subtrahend) {
	dx -= subtrahend->dx;
	dy -= subtrahend->dy;
	dz -= subtrahend->dz;
}

void RealVector3d::multiply(double scalar){
	dx *= scalar;
	dy *= scalar;
	dz *= scalar;
}

RealVector3d *RealVector3d::getMultiply(double scalar){
	RealVector3d * result = new RealVector3d();
	result->dx = dx * scalar;
	result->dy = dy * scalar;
	result->dz = dz * scalar;
	return result;
}

RealVector3d *RealVector3d::copy(){
	RealVector3d *result = new RealVector3d(this->dx, this->dy, this->dz);
	return result;
}

double RealVector3d::getMagnitude(){
	return sqrt(dx*dx + dy*dy + dz*dz);
}

double RealVector3d::dotProduct(RealVector3d * a, RealVector3d * b){ //NANs coming out of this function
	double result = 0;												 //possibly getting passed NANs. FIX
	result += a->dx*b->dx;
	result += a->dy*b->dy;
	result += a->dz*b->dz;
	return result;
}

RealVector3d * RealVector3d::crossProduct(RealVector3d* v, RealVector3d* w){
	RealVector3d * result = new RealVector3d();
	result->dx = v->dy*w->dz - v->dz*w->dy;
	result->dy = v->dz*w->dx - v->dx*w->dz;
	result->dz = v->dx*w->dy - v->dy*w->dx;
	return result;
}

double RealVector3d::angleBetween(RealVector3d * v, RealVector3d * w){ //returns in radians
	double result = RealVector3d::dotProduct(v,w);
	result /= v->getMagnitude() * w->getMagnitude();
	return acos(result);
}

double RealVector3d::angleVectorPlane(RealVector3d * vector, RealVector3d * plane){
	double top = vector->dx*plane->dx + vector->dy*plane->dy + vector->dz*plane->dz;
	double bottomLeft = sqrt(plane->dx*plane->dx + plane->dy*plane->dy + plane->dz*plane->dz);
	double bottomRight = sqrt(vector->dx*vector->dx + vector->dy*vector->dy + vector->dz*vector->dz);
	double result = top/(bottomLeft*bottomRight);
	return asin(result);
}

void RealVector3d::copy(RealVector3d *dest, RealVector3d *src){
	dest->dx = src->dx;
	dest->dy = src->dy;
	dest->dz = src->dz;
}

RealVector4d::RealVector4d(){
	dx = 0.f;
	dy = 0.f;
	dz = 0.f;
	w = 0.f;
}

void RealVector4d::normalize(){
	double magnitude = sqrt(dx*dx + dy*dy + dz*dz + w*w);
	if(magnitude != 0){
		dx /= magnitude;
		dy /= magnitude;
		dz /= magnitude;
		w /= magnitude;
	}
}

void RealVector4d::operate(RealVector4d* o){
	double tempdx = (o->w * dx) + (o->dx * w) + (o->dy * dz) - (o->dz * dy);
	double tempdy = (o->w * dy) - (o->dx * dz) + (o->dy * w) + (o->dz * dx);
	double tempdz = (o->w * dz) + (o->dx * dy) - (o->dy * dx) + (o->dz * w);
	double tempw  = (o->w * w) - (o->dx * dx) - (o->dy * dy) - (o->dz * dz);
	dx = tempdx;
	dy = tempdy;
	dz = tempdz;
	w = tempw;
	this->normalize();
}

RealMatrix3d * RealVector4d::getMatrix(){
	RealMatrix3d * result = new RealMatrix3d();
	normalize();
	const double tempMatrix[][3] = {{1 - 2*dy*dy - 2*dz*dz, 2*dx*dy + 2*dz*w, 2*dx*dz - 2*dy*w},
							 {2*dx*dy - 2*dz*w, 1 - 2*dx*dx - 2*dz*dz, 2*dy*dz + 2*dx*w},
							 {2*dx*dz + 2*dy*w, 2*dy*dz - 2*dx*w, 1 - 2*dx*dx - 2*dy*dy}};
	memcpy(&result->contents, &tempMatrix, 9*sizeof(double));
	return result;
}

RealVector4d * RealVector4d::axisAngle(RealVector3d * axis, double angle){ //angle in rads
	double sinTheta = sin(angle / 2.f);

	RealVector4d * temp = new RealVector4d();

	temp->dx = axis->dx * sinTheta;
	temp->dy = axis->dy * sinTheta;
	temp->dz = axis->dz * sinTheta;

	temp->w = cos(angle / 2.f);

	return temp;

}

RealMatrix3d::RealMatrix3d(){
	int i, j;
	for (i = 0; i < 3; i++){
		for (j = 0; j < 3; j++){
			contents[i][j] = 0.f;
		}
	}
}

GameObject::GameObject(){}

GameObject::GameObject(RealVector3d * startPoint){
	position = new RealVector3d();
	position->dx = startPoint->dx;
	position->dy = startPoint->dy;
	position->dz = startPoint->dz;
	rotation = new RealVector4d();
	rotation->dz = 1;
	xAxis = new RealVector3d();
	xAxis->dx = 1;
	yAxis = new RealVector3d();
	yAxis->dy = 1;
	zAxis = new RealVector3d();
	zAxis->dz = 1;
	acceleration = new RealVector3d(0, 0, 0);
	velocity = new RealVector3d(0, 0, 0);
	mass = 1;
	collisionRadius = 0.f;
	model = 0;
	cameraPresent = false;
}

void GameObject::reset(){
	delete position;
	delete rotation;
	delete xAxis;
	delete yAxis;
	delete zAxis;
	delete acceleration;
	delete velocity;

	position = new RealVector3d();
	rotation = new RealVector4d();
	rotation->dz = 1;
	xAxis = new RealVector3d();
	xAxis->dx = 1;
	yAxis = new RealVector3d();
	yAxis->dy = 1;
	zAxis = new RealVector3d();
	zAxis->dz = 1;
	acceleration = new RealVector3d();
	velocity = new RealVector3d();
	velocity->dz = 50;
}

void GameObject::move(RealVector3d * relativeDest){
	position->dx += relativeDest->dx;
	position->dy += relativeDest->dy;
	position->dz += relativeDest->dz;
	// printf("%f, %f, %f\n", position->dx,
	// 					   position->dy,
	// 					   position->dz);
}

void GameObject::rotateQuat(RealVector4d * rotator){
	rotation->operate(rotator);
	RealMatrix3d * matrix = rotator->getMatrix();
	xAxis->rotate(matrix);
	yAxis->rotate(matrix);
	zAxis->rotate(matrix);
	delete matrix;
}

void GameObject::rotate(RealVector3d * axis, double angle){
	RealVector4d * rotator = RealVector4d::axisAngle(axis, angle);
	rotation->operate(rotator);
	RealMatrix3d * matrix = rotator->getMatrix();
	xAxis->rotate(matrix);
	yAxis->rotate(matrix);
	zAxis->rotate(matrix);
	delete rotator;
	delete matrix;

	// printf("xAxis: %f %f %f\n\r yAxis: %f %f %f\n\r zAxis: %f %f %f\n\r",
	// 		xAxis->dx, xAxis->dy, xAxis->dz,
	// 		yAxis->dx, yAxis->dy, yAxis->dz,
	// 		zAxis->dx, zAxis->dy, zAxis->dz);
	// pitch = fmodf((pitch + p), 360.f);
	// roll = fmodf((roll + r), 360.f);
	// yaw = fmodf((yaw + y), 360.f);
	// printf("Rotated\n");
}

int GameObject::advance(double dt){
	double gravity = -9.81;

	RealVector3d totalForce;
	RealVector3d totalAccel;
	RealVector3d totalVelocity;
	RealVector3d totalMovement;

//Gravity vector
	RealVector3d gravityForce;
	gravityForce.dy = gravity*mass;

//add them together
	totalForce.add(&gravityForce);

//convert to acceleration
	totalAccel = totalForce;
	totalAccel.multiply(1.f/mass);
	memcpy(acceleration, &totalAccel, sizeof(RealVector3d));

	totalVelocity = totalAccel;
	totalVelocity.multiply(dt);
	totalVelocity.add(velocity);
	memcpy(velocity, &totalVelocity, sizeof(RealVector3d));

	totalMovement = totalVelocity;
	totalMovement.multiply(dt);

	this->move(&totalMovement);
	return 0;
}

int GameObject::getModel(){
	return this->model;
}

void GameObject::setModel(int m) {
	this->model = m;
}

double GameObject::getCollisionRadius() {
	return this->collisionRadius;
}