#include "ControlInputReceiver.hpp"
#include <assert.h>

ControlInputReceiver::ControlInputReceiver() {
	for (u32 i=0; i<KEY_KEY_CODES_COUNT; ++i) {
		KeyIsDown[i] = false;
	}

	for (int j = 0; j < NUMBER_OF_SCROLL_BARS; j++){
		scrollBarPositions[j] = 0;
	}

	mouseState.rightButtonDown = false;
	mouseState.leftButtonDown = false;
	mouseState.positionX = 0;
	mouseState.positionY = 0;
	mouseState.scrollTotal = 0;
}

void ControlInputReceiver::initialize(gui::IGUIEnvironment * env) {
	gui::IGUIElement * root = env->getRootGUIElement();
	for (int j = 0; j < NUMBER_OF_SCROLL_BARS; j++){
		scrollBarPositions[j] = 1000 - reinterpret_cast<gui::IGUIScrollBar*>(root->getElementFromId((s32)j))->getPos();
	}
}

bool ControlInputReceiver::OnEvent(const SEvent& event){
	if (event.EventType == EET_KEY_INPUT_EVENT) {
		KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
	}
	else if (event.EventType == EET_GUI_EVENT){
		if(event.GUIEvent.EventType ==gui::EGET_SCROLL_BAR_CHANGED){
			int id = event.GUIEvent.Caller->getID();
			scrollBarPositions[id] = 1000 - ((gui::IGUIScrollBar*)event.GUIEvent.Caller)->getPos();
		}
	}
	else if (event.EventType == EET_MOUSE_INPUT_EVENT) {
		switch(event.MouseInput.Event){

		case EMIE_LMOUSE_PRESSED_DOWN:
			mouseState.leftButtonDown = true;
			break;

		case EMIE_LMOUSE_LEFT_UP:
			mouseState.leftButtonDown = false;
			break;

		case EMIE_RMOUSE_PRESSED_DOWN:
			mouseState.rightButtonDown = true;
			break;

		case EMIE_RMOUSE_LEFT_UP:
			mouseState.rightButtonDown = false;
			break;

		case EMIE_MOUSE_MOVED:
			mouseState.positionX = event.MouseInput.X;
			mouseState.positionY = event.MouseInput.Y;
			break;
		case EMIE_MOUSE_WHEEL:
			mouseState.scrollTotal += -event.MouseInput.Wheel;
			break;
		}
	}

	return false;
}

bool ControlInputReceiver::IsKeyDown(EKEY_CODE keyCode) const {
	return KeyIsDown[keyCode];
}

int ControlInputReceiver::scrollBarCheck(int scrollBar) const{
	return scrollBarPositions[scrollBar];
}

MouseState ControlInputReceiver::getMouseState() {
	return mouseState;
	mouseState.scrollTotal = 0;
}