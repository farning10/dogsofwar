#ifndef MISSILEOBJECT_H
#define MISSILEOBJECT_H

#include "PlaneObject.hpp"
struct ControlSet;
class ObjectList;

class MissileObject : public PlaneObject {
public:
	MissileObject(RealVector3d*);
	void setTarget(GameObject*);
	virtual ObjectList *fireInternal(double);
	virtual int advance(double);
	virtual bool readyToFire();
	double blastRadius;
	ObjectList *bulletList;
protected:
	virtual ControlSet *align();
	virtual void burnFuel(double);
	virtual int fuse();
	double maxThrust;
	bool armed;
	double burnDuration;
	double delay;
	bool burnStarted;
	double currTime;
	double fuelMass;
	GameObject *target;
	RealVector3d prevLOS;
};

#endif