#ifndef BULLETOBJECT_H
#define BULLETOBJECT_H

#include "local.hpp"

class BulletObject : public GameObject {
public:
	BulletObject(RealVector3d*, RealVector4d*, RealVector3d*, double, double);
	void setList(ObjectList*);
	virtual int advance(double);
protected:
	double damage;
	double lifeTime;
	ObjectList *currList;
	~BulletObject();
};

#endif