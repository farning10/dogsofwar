#include "Octree.hpp"

OctreeNode::OctreeNode(DeviceHandler *d)
: objects(d)
{
	dhr = d;
	parent = nullptr;
	int i;
	for (i = 0; i < 8; i++){
		children[i] = nullptr;
	}
}

OctreeNode::OctreeNode(DeviceHandler *d, OctreeNode *p)
: objects(d)
{
	dhr = d;
	parent = p;
	int i;
	for (i = 0; i < 8; i++){
		children[i] = nullptr;
	}
}

void OctreeNode::subDivide(){ //subdivide the node into 8 children
	int i;
	for (i = 0; i < 8; i++){
		children[i] = new OctreeNode(dhr, this);
		// set correct midpont and size for the node
	}
}

Octree::Octree(DeviceHandler *dhr){
	root = new OctreeNode(dhr); //create a root node with nothing in it
}

void Octree::add(GameObject *insert){
	addWalk(root, insert);
}

void Octree::addWalk(OctreeNode *curr, GameObject *insert){
	int index;
	if (insert->position->dx < curr->midpoint->dx) {
		if (insert->position->dy < curr->midpoint->dy) {
			if (insert->position->dz < curr->midpoint->dz) {
				index = 0;
			}
			else {
				index = 1;
			}
		}
		else {
			if (insert->position->dz < curr->midpoint->dz) {
				index = 2;
			}
			else {
				index = 3;
			}
		}
	}
	else {
		if (insert->position->dy < curr->midpoint->dy) {
			if (insert->position->dz < curr->midpoint->dz) {
				index = 4;
			}
			else {
				index = 5;
			}
		}
		else {
			if (insert->position->dz < curr->midpoint->dz) {
				index = 6;
			}
			else {
				index = 7;
			}
		}
	}

	if(children[index]){ //child node exists
		addWalk(children[index], insert);
	}
	else {
		if (curr->objects.add(insert) ) {
			curr->subDivide();
		}
	}
}