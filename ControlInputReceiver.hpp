#ifndef CONTROLINPUTRECEIVER_H
#define CONTROLINPUTRECEIVER_H

#include <irrlicht.h>
#include "definitions.hpp"

using namespace irr;
struct MouseState {
	bool rightButtonDown;
	bool leftButtonDown;
	int positionX;
	int positionY;
	double scrollTotal;
};

class ControlInputReceiver : public IEventReceiver
{
public:
	virtual bool OnEvent(const SEvent&);
	virtual bool IsKeyDown(EKEY_CODE) const;
	virtual int scrollBarCheck(int) const;
	virtual MouseState getMouseState();

	ControlInputReceiver();
	virtual void initialize(gui::IGUIEnvironment*);

private:
	bool KeyIsDown[KEY_KEY_CODES_COUNT];
	int scrollBarPositions[NUMBER_OF_SCROLL_BARS];
	MouseState mouseState;
};

#endif