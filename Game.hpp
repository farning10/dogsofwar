//Dependant on: GameObject, GameSettings, GameObjectList

#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <stdlib.h>
#include "local.hpp"

class Game{
public:
	Game(DeviceHandler*);
	void play();

protected:
	int cycle(double);

	DeviceHandler *deviceHandler;
	ControlInputReceiver *cir;

	MouseState currMS;
	MouseState prevMS;

	bool camCheck;
	bool lockCheck;
	bool switchCheck;
	bool spaceCheck;
	bool rCheck;
	bool physics;

	double rotSpeed;
	PlayerObject *player;
	ObjectList planes;
	ObjectList bullets;
	ObjectList missiles;

	int input(double);
	int advanceGame(double);
	int render();
};

#endif