#include "AIObject.hpp"
#include <math.h>
#include <stdlib.h>

Manuever::Manuever()
{
	type = 0;
	memset(md, 0, sizeof(md));
}

AIObject::AIObject(RealVector3d *start, ObjectList *pls, ObjectList *bts)
: PlaneObject(start), manuever()
{
	planes = pls;
	bullets = bts;
	manuever.type = 1;
	manuever.md[1].position = new RealVector3d();
	manuever.md[2].floating = 1.f;
	manuever.md[3].floating = 5.f;
}

int AIObject::advance(double dt) {
	makeDecision(dt);
	PlaneObject::advance(dt);
//	printf("%f, %f\n", this->throttle, this->velocity->dy);
	return 0;
}


void AIObject::makeDecision(double dt){ //basically decides which manuever to do
	//analyze immidiate threats
	//if immediate threats detected, switch manuevers as appropriate
	//if none call navigate
	
	// find target:
	planes->reset();
	GameObject *curr;
	while (curr = planes->get()) {
		if(curr != this) {
			manuever.md[0].gameObject = curr;
			break;
		}
	}
	memcpy(manuever.md[1].position, manuever.md[0].gameObject->position, sizeof(RealVector3d));
	ControlSet *controls = navigate(&manuever, dt);
	if (controls->trigger) {
		ObjectList *temp = this->fireInternal(dt);
		bullets->merge(temp);
	}
	this->passControl(controls);
}

ControlSet* AIObject::navigate(Manuever *cm, double dt){ //return a control set based on manuever passed
	ControlSet* result;
	switch(cm->type){
	case 1: //align nose
		result = align(cm, dt);
		break;
	case 2:
		result = guns(cm, dt);
	default: //steady flight
		//do whatever
		break;
	}
}

ControlSet* AIObject::align(Manuever *cm, double dt){ //md[0] is target object pointer, 1 is target position pointer, 2 is throttle setting
	ControlSet *result = new ControlSet();
	RealVector3d path;
	memcpy(&path, cm->md[1].position, sizeof(RealVector3d));
	path.subtract(position);
	RealVector3d *ppath = &path;
//	printf("%f\n", path.getMagnitude());
//	printf("%f, %f, %f\n", zAxis->dx, zAxis->dy, zAxis->dz);
//	printf("%f\n", RealVector3d::dotProduct(ppath, zAxis));

	double angle = acos(RealVector3d::dotProduct(ppath, yAxis) / (path.getMagnitude()*yAxis->getMagnitude())); //in rads
	RealVector3d *axis = RealVector3d::crossProduct(ppath, zAxis);
	double rotationAngle = acos(RealVector3d::dotProduct(axis, yAxis) / (axis->getMagnitude()*xAxis->getMagnitude()));
	double dRot = (rotationAngle - (M_PI/2)) / M_PI;
	double dPitch = (angle - (M_PI/2)) / -M_PI;
	if (rotationAngle == rotationAngle) {
		if(dRot > 0.005) {
			result->roll = 1;
		}
		else if (dRot < 0.005) {
			result->roll = -1;
		}
		else result->roll = 0;
	}
//	printf("%f, %f\n", rotationAngle, result->roll);
	if (angle == angle) {
		if(dPitch > 0.005 ) {
			result->pitch = 1;
		}
		else if(dPitch < 0.005 ) {
			result->pitch = -1;
		}
		else result->pitch = 0;
	}
//	printf("%f, %f\n", result->roll, result->pitch);
//	printf("%f, %f, %f\n", axis->dx, axis->dy, axis->dz);
	result->throttle = cm->md[2].floating;
	if(dPitch < 0.01 && dPitch > -0.01 && dRot < 0.01 && dRot > -0.01){
		result->trigger = 1;
	}
	else result->trigger = 0;
//	system("PAUSE");
	delete axis;

	cm->md[3].floating -= dt;
	if (cm->md[3].floating <= 0) cm->type = 2;
	return result;
}

ControlSet* AIObject::guns(Manuever *cm, double dt){  //shoot down the opposing plane
	double y = cm->md[0].gameObject->position->dy - this->position->dy;
	double me[2] = {this->position->dx, this->position->dz};
	double enemy[2] = {cm->md[0].gameObject->position->dx, cm->md[0].gameObject->position->dz};
	double x = sqrt((enemy[0] - me[0])*(enemy[0] - me[0]) + (enemy[1] - me[1])*(enemy[1] - me[1]));
	double travelTime = x/(muzzleVelocity + this->velocity->getMagnitude() - cm->md[0].gameObject->velocity->getMagnitude());
	//x += travelTime*cm->md[0].gameObject->velocity->getMagnitude();
	//printf("%f\n", x);
	//x += (rand() % static_cast<int>(x)) - (x/2);
	double vsqrd = (this->muzzleVelocity + this->velocity->getMagnitude())*(this->muzzleVelocity + this->velocity->getMagnitude());
	double tangent = vsqrd - sqrt(vsqrd*vsqrd - 9.81*(9.81*x*x + 2*y*vsqrd));
	tangent /= 9.81*x;
	double theta = atan(theta);

	RealVector3d target;
	memcpy(&target, this->position, sizeof(RealVector3d));
	RealVector3d path(enemy[0]-me[0], 0, enemy[1]-me[1]);
	path.normalize();
	path.multiply(x);
	cm->md[1].position->dx = target.dx + path.dx;
	cm->md[1].position->dy = target.dy + tangent*x*2;
	cm->md[1].position->dz = target.dz + path.dz;

	//printf("%f\n", tangent*x*2 - y);

	ControlSet *result = align(cm, dt);
	return result;
}