#include "PlaneObject.hpp"
#include "BulletObject.hpp"
#include <math.h>

Pylon::Pylon(RealVector3d *pos, MissileObject *payload){
	load = payload;
	memcpy(&position, pos, sizeof(RealVector3d));
}

Pylon::Pylon(){} //do not use!

bool Pylon::ready(){
	if (load) {
		if (load->readyToFire()) return 1;
	}
	return 0;
}

ControlSet::ControlSet(){
	throttle = 0;
	pitch = 0;
	roll = 0;
	yaw = 0;
	trigger = 0;
	weaponRelease = 0;
}

ControlSet::ControlSet(double t, double p, double r, double y){
	throttle = t;
	pitch = p;
	roll = r;
	yaw = y;
	trigger = 0;
	weaponRelease = 0;
}

//public
PlaneObject::PlaneObject(RealVector3d * startPoint)
: centerOfLift(0, 0, -1.5), 
  centerOfMass(0, 0, -2),
  centerOfThrust(0, 0, -7)
{
	model = 1;
	cameraPresent = false;

	position = new RealVector3d();
	position->dx = startPoint->dx;
	position->dy = startPoint->dy;
	position->dz = startPoint->dz;

	rotation = new RealVector4d();
	rotation->dz = 1;

	xAxis = new RealVector3d();
	xAxis->dx = 1;
	yAxis = new RealVector3d();
	yAxis->dy = 1;
	zAxis = new RealVector3d();
	zAxis->dz = 1;

	acceleration = new RealVector3d();
	velocity = new RealVector3d();

	velocity->dz = 194;
	thrust = 76300;
	mass = 12000;
	liftEfficiency = 5.f;
	averageStaticDrag = 1.f;
	verticalStabilizer = 0.00001f; //radians
	mPitch = 0.5f;
	mRoll = 1.f;
	mYaw = 0.2f;
	collisionRadius = 7.5f;

	wingArea = 27.87f;
	tailArea = 5.f;

	cPitch = 0.f;
	cRoll = 0.f;
	cYaw = 0.f;
	throttle = 0.f;

	ROF = 6000; //6000
	fireOffset = 0; //always 0
	muzzleVelocity = 1050;//1050
	projectileMass = 0.1024;
	gunPosition.dx = 0;
	gunPosition.dy = 0;
	gunPosition.dz = 8; //subject to change!
}

PlaneObject::PlaneObject() {}

void PlaneObject::deepCopy(PlaneObject * target){
	delete position;
	delete rotation;
	delete xAxis;
	delete yAxis;
	delete zAxis;
	delete acceleration;
	delete velocity;
	position = new RealVector3d();
	memcpy(position, target->position, sizeof(RealVector3d));
	rotation = new RealVector4d();
	memcpy(rotation, target->rotation, sizeof(RealVector4d));
	xAxis = new RealVector3d();
	memcpy(xAxis, target->xAxis, sizeof(RealVector3d));
	yAxis = new RealVector3d();
	memcpy(yAxis, target->yAxis, sizeof(RealVector3d));
	zAxis = new RealVector3d();
	memcpy(zAxis, target->zAxis, sizeof(RealVector3d));
	acceleration = new RealVector3d();
	memcpy(acceleration, target->acceleration, sizeof(RealVector3d));
	velocity = new RealVector3d();
	memcpy(velocity, target->velocity, sizeof(RealVector3d));

	thrust = target->thrust;
	mass = target->mass;
	liftEfficiency = target->liftEfficiency;
	verticalStabilizer = target->verticalStabilizer; //radians
	mPitch = target->mPitch;
	mRoll = target->mRoll;
	mYaw = target->mYaw;

	wingArea = target->wingArea;
	tailArea = target->tailArea;

	cPitch = target->cPitch;
	cRoll = target->cRoll;
	cYaw = target->cYaw;
	throttle = target->throttle;
}

int PlaneObject::advance(double dt){
	int iterations = (int)(dt / MOVEMENT_PRECISION);
	double fraction = fmod(dt, MOVEMENT_PRECISION);
	advanceMotion(fraction);
	advanceRotation(fraction);
	int i = 0;
	while (i < iterations){
		advanceMotion(MOVEMENT_PRECISION);
		advanceRotation(MOVEMENT_PRECISION);
		i++;
	}
	return 0;
}

void PlaneObject::setControl(ControlSet *controls){
	this->throttle = controls->throttle;
	cPitch = controls->pitch;
	//if(cPitch > 0) cPitch *= 1.1;
	cRoll = controls->roll;
	cYaw = controls->yaw;
}

void PlaneObject::passControl(ControlSet *controls){
	this->setControl(controls);
	delete controls;
}

ObjectList * PlaneObject::fireInternal(double dt){
	double cycleTime = 60.f / ROF;
	ObjectList *newBullets = new ObjectList();
	RealVector3d firePos;
	RealVector3d *fireVelocity = nullptr;
	memcpy(&firePos, &gunPosition, sizeof(RealVector3d));
	RealMatrix3d *rotationM = rotation->getMatrix();
	firePos.rotate(rotationM);
	delete rotationM;
	fireVelocity = firePos.getMultiply(muzzleVelocity);
	fireVelocity->add(velocity);
	firePos.add(position);

	BulletObject *currBullet;
	double currOffset = fireOffset + dt;
	if(currOffset >= cycleTime) {
		currBullet = new BulletObject(&firePos, rotation, fireVelocity, projectileMass, 1.f);
		newBullets->add(currBullet);
		newBullets->advanceAll(cycleTime - fireOffset);
		currOffset -= cycleTime;
	}
	while (currOffset >= cycleTime) {
		currBullet = new BulletObject(&firePos, rotation, fireVelocity, projectileMass, 1.f);
		newBullets->add(currBullet);
		newBullets->advanceAll(cycleTime);
		currOffset -= cycleTime;
	}

	fireOffset = currOffset;
	delete fireVelocity;
	return newBullets;
}

void PlaneObject::advanceMotion(double dt){
	double gravity = -9.81;

	RealVector3d totalForce;
	RealVector3d totalAccel;
	RealVector3d totalVelocity;
	RealVector3d totalMovement;

//Gravity vector
	RealVector3d gravityForce;
	gravityForce.dy = gravity*mass;

//Thrust vector
	RealVector3d thrustForce;
	thrustForce.dx = zAxis->dx;
	thrustForce.dy = zAxis->dy;
	thrustForce.dz = zAxis->dz;
	thrustForce.multiply(thrust*throttle);

//Lift vector - sum of all aerodynamic forces
	double zVelocity = RealVector3d::dotProduct(velocity, zAxis);
	RealVector3d * liftForce = getLift();

	RealVector3d dragForce;
	RealVector3d *temp = this->velocity->getNormalize();
	memcpy(&dragForce, temp, sizeof(RealVector3d));
	delete temp;
	double AOA = RealVector3d::angleBetween(zAxis, velocity);
	double dragMultiplier = sin(AOA);
	if (this->velocity->getMagnitude() >= 50) dragForce.multiply(-0.5*velocity->getMagnitude()*velocity->getMagnitude()*averageStaticDrag);
	else (dragForce.multiply(-0.1*this->velocity->getMagnitude()));
	dragForce.multiply(dragMultiplier*10+1);

//add them together
	totalForce.add(&gravityForce);
	totalForce.add(&thrustForce);
	totalForce.add(liftForce);
	totalForce.add(&dragForce);
	delete liftForce;

//convert to acceleration
	totalAccel = totalForce;
	totalAccel.multiply(1.f/mass);
	memcpy(acceleration, &totalAccel, sizeof(RealVector3d));

	totalVelocity = totalAccel;
	totalVelocity.multiply(dt);
	totalVelocity.add(velocity);
	memcpy(velocity, &totalVelocity, sizeof(RealVector3d));

	totalMovement = totalVelocity;
	totalMovement.multiply(dt);

	this->move(&totalMovement);
}

void PlaneObject::advanceRotation(double dt){
	double wingAngle = RealVector3d::angleVectorPlane(velocity, yAxis);
	double tailAngle = RealVector3d::angleVectorPlane(velocity, xAxis);

	double wingForce = cos(wingAngle);
	double tailForce = cos(tailAngle);

	double pitch = -cPitch * mPitch * dt * wingForce * velocity->getMagnitude() * velocity->getMagnitude() / 115600;
	double roll = -cRoll * mRoll * dt;
	double yaw = cYaw * mYaw * dt;

	this->rotate(xAxis, pitch);
	this->rotate(zAxis, roll);
	this->rotate(yAxis, yaw);
}

RealVector3d * PlaneObject::getLift(){

	RealVector3d * horizontalPlane = RealVector3d::crossProduct(xAxis, zAxis);
	RealVector3d * verticalPlane = RealVector3d::crossProduct(yAxis, zAxis);
	double aoa = RealVector3d::angleVectorPlane(velocity, horizontalPlane);
	double hAoa = RealVector3d::angleVectorPlane(velocity, verticalPlane);
	double liftCoefficient = aoa*2*M_PI;
	double hLiftCoefficient = hAoa*2*M_PI;
	double speed = velocity->getMagnitude();

	if (aoa >= M_PI / 9 /*20 degrees*/|| speed < 100) {
		delete horizontalPlane;
		delete verticalPlane;
		return new RealVector3d(0,0,0);
	}

	RealVector3d * unitVelocity = velocity->getNormalize();

	RealVector3d * lift = RealVector3d::crossProduct(unitVelocity, xAxis);
	lift->multiply(((speed*speed*liftCoefficient)/2)*wingArea);
	RealVector3d * drag = new RealVector3d(unitVelocity->dx, unitVelocity->dy, unitVelocity->dz);
	drag->multiply(-lift->getMagnitude()/liftEfficiency);

	RealVector3d * hLift = RealVector3d::crossProduct(unitVelocity, yAxis);
	hLift->multiply(((speed*speed*hLiftCoefficient)/2)*tailArea);
	RealVector3d * hDrag = new RealVector3d(unitVelocity->dx, unitVelocity->dy, unitVelocity->dz);
	hDrag->multiply(-hLift->getMagnitude()/liftEfficiency);

	double tailAngle = RealVector3d::angleVectorPlane(velocity, verticalPlane);
	double rotForce = cos(tailAngle + M_PI/2);
	this->rotate(yAxis, -rotForce*verticalStabilizer*speed);

//	printf("AOA: %f\ndrag: %f\nspeed: %f\n\n\n\n", aoa, drag->getMagnitude(), speed);

	RealVector3d * total = new RealVector3d();

	total->add(lift);
	total->add(drag);
	total->add(hLift);
	total->add(hDrag);

	delete horizontalPlane;
	delete verticalPlane;
	delete unitVelocity;
	delete drag;
	delete hLift;
	delete hDrag;
	delete lift;

	return total;
}

int PlaneObject::newAdvanceProto(double dt)
{
	RealVector3d sumForces(0,0,0); // sum of linear forces
	RealVector3d sumTorque(0,0,0); //sum of angular forces
	Force *curr;
	while (curr = forceStack.pop())
	{
		RealVector3d *force = &curr->force;
		RealVector3d *arm = &curr->arm;
		arm->subtract(&centerOfMass);

		RealVector3d *tempTorque = RealVector3d::crossProduct(arm, force);
		RealVector3d torque;
		memcpy(&torque, tempTorque, sizeof(RealVector3d));
		delete tempTorque;

		sumForces.add(force);
		sumTorque.add(&torque);
		delete curr;
	}
	RealVector3d *tempAcceleration = sumForces.getMultiply(1/mass);
	memcpy(acceleration, tempAcceleration, sizeof(RealVector3d));
	
	double momentOfInertia = (2/5)*mass*collisionRadius*collisionRadius;
	RealVector3d *tempAngAccel = sumTorque.getMultiply(1/momentOfInertia);
	memcpy(&angularAcceleration, tempAngAccel, sizeof(RealVector3d));

	RealVector3d *dVelocity = acceleration->getMultiply(dt);
	RealVector3d *dAngVel = angularAcceleration.getMultiply(dt);
	velocity->add(dVelocity);
	angularVelocity.add(dAngVel);
	delete dVelocity;
	delete dAngVel;

	RealVector3d movement;
	RealVector3d angMovement;
	RealVector3d *tempMovement = velocity->getMultiply(dt);
	RealVector3d *tempAngMovement = angularVelocity.getMultiply(dt);
	memcpy(&movement, tempMovement, sizeof(RealVector3d));
	memcpy(&angMovement, tempAngMovement, sizeof(RealVector3d));
	delete tempMovement;
	delete tempAngMovement;

	this->move(&movement);
	this->rotate(&angMovement, angMovement.getMagnitude());
}