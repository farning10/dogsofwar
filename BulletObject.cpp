#include "BulletObject.hpp"

BulletObject::BulletObject(RealVector3d* startPos, RealVector4d* startRot, RealVector3d* iVelocity, double kgs, double dmg)
: GameObject(startPos)
{
	mass = kgs;
	damage = dmg;
	model = 2;
	lifeTime = 0.f;
	this->rotateQuat(startRot);
	memcpy(velocity, iVelocity, sizeof(RealVector3d));
}

int BulletObject::advance(double dt){
	GameObject::advance(dt);
	lifeTime += dt;
	if (lifeTime > 5.f){
		return 1;
	}
	else return 0;
}