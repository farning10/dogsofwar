#include "DeviceHandler.hpp"
#include "math.h"

using namespace irr;

Item::Item(){
	next = nullptr;
	payload = nullptr;
}

DeviceHandler::DeviceHandler(int width, int height, bool fullscreen){

	//make the event receiver
	cir = new ControlInputReceiver();

	SIrrlichtCreationParameters cprs;
	cprs.AntiAlias = ANTI_ALIASING;
	cprs.Bits = 32;
	cprs.Doublebuffer = true;
	cprs.EventReceiver = cir;
	cprs.Fullscreen = fullscreen;
	cprs.WindowSize = core::dimension2d<u32>(width, height);
	cprs.WithAlphaChannel = true;
	cprs.DriverType = video::EDT_OPENGL;

	device = createDeviceEx(cprs);
	driver = device->getVideoDriver();
	smgr = device->getSceneManager();
	env = device->getGUIEnvironment();
	listHead = nullptr;
	listTail = nullptr;

	camFOV = 1.57f;// radians (90 degrees)

	planeMesh = smgr->getMesh("../media/corrected_f16.obj");
	planeTexture = driver->getTexture("../media/from_store/f16.png");
	planeTextureTransparent = driver->getTexture("../media/alternate_trans_texture.png");
	bulletMesh = smgr->getMesh("../media/simpleBullet.obj");
	bulletTexture = driver->getTexture("../media/lead.jpg");
	boxTexture = driver->getTexture("../media/wall.bmp");
}

bool DeviceHandler::deviceExists(){
	if(device == 0) return false;
	else return true;
}

video::IVideoDriver * DeviceHandler::getDriver(){
	return driver;
}

scene::ISceneManager * DeviceHandler::getSceneManager(){
	return smgr;
}

gui::IGUIEnvironment * DeviceHandler::getEnvironment(){
	return env;
}

void DeviceHandler::setMouseInvisible(){
	device->getCursorControl()->setVisible(false);
}

bool DeviceHandler::run(){
	return device->run();
}

u32 DeviceHandler::getTime(){
	return device->getTimer()->getTime();
}

void DeviceHandler::stopAll(){
	device->closeDevice();
	device->run();
	device->drop();
}

void DeviceHandler::setup(){
	setupGUI();
	cir->initialize(env);

	printf("1\n");

	camera = smgr->addCameraSceneNode();
	camera->setFarValue(RENDER_DISTANCE);
	camera->setFOV(camFOV);
	camera->bindTargetAndRotation(true);
	int distance = 100;

	scene::ISceneNode * cube;

	int i, j;
	for (i = 1; i <= 200; i++){
		for(j = -100; j <= 100; j++){
			cube = smgr->addCubeSceneNode();
			cube->setPosition(core::vector3df(j*distance, -400, i*distance));
			cube->setMaterialTexture(0, boxTexture);
			cube->setMaterialFlag(video::EMF_LIGHTING, false);
		}
	}

	// Item * result = this->addObject(add);
	// return result;
}

void DeviceHandler::setupGUI(){
	gui::IGUIStaticText * temp;

	temp = env->addStaticText(L"Throttle", core::rect<s32>(WIDTH-140, HEIGHT-340, WIDTH-80, HEIGHT-320));
	temp->setTextAlignment(gui::EGUIA_CENTER, gui::EGUIA_CENTER);
	gui::IGUIScrollBar * throttle = env->addScrollBar(
				false, core::rect<s32>(WIDTH-120, HEIGHT-300, WIDTH-100, HEIGHT-100), 0, GUI_ID_THROTTLE);
	throttle->setMax(1000);
	throttle->setPos(0);

	temp = env->addStaticText(L"Pitch", core::rect<s32>(WIDTH-260, HEIGHT-340, WIDTH-200, HEIGHT-320));
	temp->setTextAlignment(gui::EGUIA_CENTER, gui::EGUIA_CENTER);
	gui::IGUIScrollBar * pitch = env->addScrollBar(
				false, core::rect<s32>(WIDTH-240, HEIGHT-300, WIDTH-220, HEIGHT-100), 0, GUI_ID_PITCH);
	pitch->setMax(1000);
	pitch->setPos(500);

	temp = env->addStaticText(L"Roll", core::rect<s32>(WIDTH-540, HEIGHT-160, WIDTH-340, HEIGHT-140));
	temp->setTextAlignment(gui::EGUIA_CENTER, gui::EGUIA_CENTER);
	gui::IGUIScrollBar * roll = env->addScrollBar(
				true, core::rect<s32>(WIDTH-540, HEIGHT-120, WIDTH-340, HEIGHT-100), 0, GUI_ID_ROLL);
	roll->setMax(1000);
	roll->setPos(500);

	temp = env->addStaticText(L"Rudder", core::rect<s32>(WIDTH-540, HEIGHT-340, WIDTH-340, HEIGHT-320));
	temp->setTextAlignment(gui::EGUIA_CENTER, gui::EGUIA_CENTER);
	gui::IGUIScrollBar * yaw = env->addScrollBar(
				true, core::rect<s32>(WIDTH-540, HEIGHT-300, WIDTH-340, HEIGHT-280), 0, GUI_ID_YAW);
	yaw->setMax(1000);
	yaw->setPos(500);
}

void DeviceHandler::draw(){
	
	driver->beginScene(true, true, video::SColor(255,113,113,133));

	smgr->drawAll(); //draw the 3d scene
	env->drawAll(); //draw the gui overlay

	driver->endScene();

	int fps = driver->getFPS();

	core::stringw tmp(L"Movement Example - Irrlicht Engine [");
	tmp += driver->getName();
	tmp += L"} fps: ";
	tmp += fps;

	device->setWindowCaption(tmp.c_str());
}

ControlInputReceiver * DeviceHandler::getReceiver(){
	return cir;
}

Item * DeviceHandler::addObject(GameObject * add){
	Item * newItem = new Item();
	newItem->gameObject = add;
	scene::ISceneNode *newNode = nullptr;
	//create an ISceneNode from the provided GameObject
	scene::IMesh *currMesh;
	video::ITexture *currTexture;
	double scale = 1;

	switch(add->getModel()){
	case 1: //plane
		currMesh = planeMesh;
		currTexture = planeTexture;
		scale = 7;
		break;
	case 2: //bullet
		currMesh = bulletMesh;
		currTexture = bulletTexture;
		scale = 0.5f;//plane scale over 75
//		printf("created a bullet\n");
		break;
	case 3: //transperent plane
		currMesh = planeMesh;
		currTexture = planeTextureTransparent;
		scale = 7;
		break;
	case 4: //missile
		currMesh = bulletMesh;
		currTexture = bulletTexture;
		scale = 1.5;
		break;
	default:
		return nullptr;
		break;
	}
//	printf("hello\n");
	newNode = smgr->addMeshSceneNode(currMesh);
//	printf("hello again\n");
	if(newNode){
		newNode->setScale(core::vector3df(scale,scale,scale));
		newNode->setPosition(core::vector3df(add->position->dx,
											 add->position->dy, 
											 add->position->dz));

		newNode->setRotation(core::vector3df(0.f, 0.f, 0.f));

		newNode->setMaterialTexture(0, currTexture);
		if (add->getModel() == 3) newNode->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL);
		newNode->setMaterialFlag(video::EMF_LIGHTING, false);
	}

	newItem->payload = newNode;
	this->insert(newItem);
	return newItem;
}

void DeviceHandler::drawLine(RealVector3d * s, RealVector3d * e){
	core::vector3df start(s->dx, s->dy, s->dz);
	core::vector3df end(e->dx, e->dy, e->dz);

	driver->draw3DLine(start, end);
}

void DeviceHandler::insert(Item * insert){
	if(!listHead){
		listHead = insert;
		listTail = listHead;
	}
	else{
		listTail->next = insert;
		listTail = listTail->next;
	}
}

void DeviceHandler::update(Item *change){
	scene::ISceneNode * node = change->payload;
	GameObject * gameObject = change->gameObject;
	if(node){
		node->setPosition(core::vector3df(gameObject->position->dx,
										  gameObject->position->dy,
										  gameObject->position->dz
										  ));
		core::quaternion original(gameObject->rotation->dx,
								  gameObject->rotation->dy,
								  gameObject->rotation->dz,
								  gameObject->rotation->w
								  );

		core::vector3df newInRads;
		original.toEuler(newInRads);
		node->setRotation(newInRads * core::RADTODEG);

		if(gameObject->cameraPresent){
			if(reinterpret_cast<PlayerObject*>(gameObject)->gunCam){
				RealVector3d relativePos(0, -5, 10);
				relativePos.rotateQuat(gameObject->rotation);
				relativePos.add(gameObject->position);
				core::vector3df absolutePos(relativePos.dx, relativePos.dy, relativePos.dz);
				camera->setPosition(absolutePos);
				camera->updateAbsolutePosition();

				RealVector4d cameraQuaternion;
				memcpy(&cameraQuaternion, gameObject->rotation, sizeof(RealVector4d));
				RealVector4d *oper8tor = RealVector4d::axisAngle(gameObject->xAxis, M_PI);
				cameraQuaternion.operate(oper8tor);
				delete oper8tor;
				core::quaternion camOriginal(cameraQuaternion.dx,
											  cameraQuaternion.dy,
											  cameraQuaternion.dz,
											  cameraQuaternion.w
											  );
				core::vector3df camInRads;
				original.toEuler(camInRads);
				camera->setRotation(camInRads * core::RADTODEG);

				core::vector3df up(gameObject->yAxis->dx, gameObject->yAxis->dy, gameObject->yAxis->dz);
				camera->setUpVector(up);
			}
			else {
				RealVector3d * cameraOffset = new RealVector3d();
				cameraOffset->add(reinterpret_cast<PlayerObject*>(gameObject)->cameraAngle);
				cameraOffset->multiply(reinterpret_cast<PlayerObject*>(gameObject)->cameraDistance);
				cameraOffset->add(gameObject->position);

				camera->setPosition(core::vector3df(cameraOffset->dx,
													cameraOffset->dy,
													cameraOffset->dz
													));
				delete cameraOffset;
				camera->setTarget(core::vector3df(gameObject->position->dx,
												  gameObject->position->dy,
												  gameObject->position->dz
												  ));
				camera->setUpVector(core::vector3df(0,1,0));
			}
		}
	}
}

void DeviceHandler::remove(Item *rm) {
	rm->payload->remove();
}