#ifndef LOCAL_H
#define LOCAL_H

#include "definitions.hpp"
#include "GameObject.hpp"
#include "ControlInputReceiver.hpp"
#include "DeviceHandler.hpp"
#include "PlaneObject.hpp"
#include "PlayerObject.hpp"
#include "AIObject.hpp"
#include "MissileObject.hpp"
#include "BulletObject.hpp"
#include "ObjectList.hpp"


#endif