#ifndef AIOBJECT_H
#define AIOBJECT_H

#include "PlaneObject.hpp"

union ManueverData {
	double floating;
	long fixed;
	GameObject *gameObject;
	RealVector3d *position;
};

struct Manuever{
	Manuever();
	int type; // 0 for no manuever (steady flight), other manuever numbers to be determined
	ManueverData md[5];
};

class AIObject : public PlaneObject {
public:
	AIObject(RealVector3d*, ObjectList*, ObjectList*);
	virtual int advance(double);
private:
	virtual void makeDecision(double);
	virtual ControlSet* navigate(Manuever*, double); //creates new pointer
	virtual ControlSet* align(Manuever*, double);
	virtual ControlSet* guns(Manuever*, double);
	ObjectList *planes;
	ObjectList *bullets;
	Manuever manuever; //current manuever, null if no manuever is currently being executed
	double manueverTime; //amount of time elapsed in current manuever
};

#endif