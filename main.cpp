#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "definitions.hpp"
#include "Game.hpp"
#include "ControlInputReceiver.hpp"
#include "DeviceHandler.hpp"
#include "GameObject.hpp"
#include "PlaneObject.hpp"
#include "PlayerObject.hpp"
#include "ObjectList.hpp"

int main(){

	srand(time(NULL));

	DeviceHandler * deviceHandler = new DeviceHandler(WIDTH, HEIGHT, FULLSCREEN);

	if (!deviceHandler->deviceExists()){
		printf("device failure\n");
		return 1;
	}

	Game game(deviceHandler);

	game.play();

	system("PAUSE");
	return 0;
}