#ifndef PLANEOBJECT_H
#define PLANEOBJECT_H

#include "definitions.hpp"
#include "GameObject.hpp"
#include "ObjectList.hpp"
#include "ForceStack.hpp"

class ObjectList;
class BulletObject;
class MissileObject;

struct Pylon
{
	Pylon(RealVector3d*, MissileObject*);
	Pylon();
	bool ready();
	RealVector3d position; //always relative to the position of the plane
	MissileObject *load;
};

struct ControlSet
{
	ControlSet();
	ControlSet(double, double, double, double);
	double throttle;
	double pitch;
	double roll;
	double yaw;
	bool trigger;
	bool weaponRelease;
};

class PlaneObject : public GameObject {
public:
	PlaneObject(RealVector3d*);
	PlaneObject();
	virtual int advance(double); //Override
	virtual int newAdvanceProto(double);
	virtual void setControl(ControlSet*);
	virtual void passControl(ControlSet*);
	virtual void deepCopy(PlaneObject*);
	virtual ObjectList *fireInternal(double);

protected:
	virtual RealVector3d *getLift(); //returns lift force vector in newtons
	virtual void advanceMotion(double);
	virtual void advanceRotation(double);

	ForceStack forceStack; // all forces for the next physics cycle will be pushed to this stack
	RealVector3d angularVelocity; // radians per second
	RealVector3d angularAcceleration; //radians per second^2

	RealVector3d centerOfLift; //all relative to the center of model "position"
	RealVector3d centerOfMass;
	RealVector3d centerOfThrust;

	double thrust; //max forward force in newtons
	double throttle; // amount of max throttle in use, between 0 and 1
	double liftEfficiency; //lift to drag ration
	double verticalStabilizer; //relative size of the vertical stabilazer
	double averageStaticDrag; //average drag of aircraft

	double wingArea; //meters squared
	double tailArea; //meters squared

	double cPitch; // current amount of control surface set
	double cRoll;  // all between 1 and -1
	double cYaw;

	double mPitch; // the effectiveness of each control surface
	double mRoll;  // in radians per second if the control is fully on
	double mYaw;

	double ROF; //(rate of fire) in rounds per minute
	double fireOffset; //seconds
	double muzzleVelocity; //meters per second
	double projectileMass; //kg
	RealVector3d gunPosition; //relative to the center of the aircraft
};

#endif