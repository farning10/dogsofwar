#include "ForceStack.hpp"

Force::Force(){}

Force::Force(RealVector3d *f, RealVector3d *a)
{
	memcpy(&force, f, sizeof(RealVector3d));
	memcpy(&arm, a, sizeof(RealVector3d));
}

ForceItem::ForceItem(RealVector3d *f, RealVector3d *a)
{
	force = new Force(f, a);
	next = nullptr;
}

ForceItem::ForceItem(Force *f)
{
	force = f;
	next = nullptr;
}

ForceStack::ForceStack()
{
	head = nullptr;
}

void ForceStack::push(RealVector3d *force, RealVector3d *arm)
{
	ForceItem *insert = new ForceItem(force, arm);
	insert->next = head;
	head = insert;
}

void ForceStack::push(Force *force)
{
	ForceItem *insert = new ForceItem(force);
	insert->next = head;
	head = insert;
}

Force *ForceStack::pop()
{
	if(head)
	{
		Force *result = head->force;
		ForceItem *temp = head;
		head = head->next;
		delete temp;
		return result;
	}
	return nullptr;
}